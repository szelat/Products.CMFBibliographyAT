## Script (Python) "getAuthorDataFromMember"
##bind container=container
##bind context=context
##bind namespace=
##bind script=script
##bind subpath=traverse_subpath
##parameters=author
##title=
##

authorschema = author.Schema()
fname = ''
if 'firstname' in authorschema:
    fname = getattr(author, authorschema['firstname'].accessor)()
elif 'given_name' in authorschema:
    fname = getattr(author, authorschema['given_name'].accessor)()

mname = ''
if 'middlename' in authorschema:
    mname = getattr(author, authorschema['middlename'].accessor)()

lname = ''
if 'lastname' in authorschema:
    lname = getattr(author, authorschema['lastname'].accessor)()
elif 'surname' in authorschema:
    lname = getattr(author, authorschema['surname'].accessor)()

hpage = ''
if 'homepage' in authorschema:
    hpage = getattr(author, authorschema['homepage'].accessor)()
elif 'home_page' in authorschema:
    hpage = getattr(author, authorschema['home_page'].accessor)()
else:
    hpage = '/'+context.portal_url.getRelativeContentURL(author)

if not fname and not lname:
    if 'fullname' in authorschema:
        fullname = getattr(author, authorschema['fullname'].accessor)().split(' ')
        fname = ' '.join(fullname[:-1])
        lname = ' '.join(fullname[-1:])

return lname and {'firstname':fname, 'middlename':mname, 'lastname':lname, 'homepage':hpage} or None
