# -*- encoding: utf-8 -*-
#######################################################
#                                                     #
#    Copyright (C), 2004, Raphael Ritz                #
#    <r.ritz@biologie.hu-berlin.de>                   #
#                                                     #
#    Humboldt University Berlin                       #
#                                                     #
# Copyright (C), 2005, Logilab S.A. (Paris, FRANCE)   #
# http://www.logilab.fr/ -- mailto:contact@logilab.fr #
#                                                     #
#######################################################

__revision__ = '$Id:  $'


import os, sys

if __name__ == '__main__':
    exec(compile(open(os.path.join(sys.path[0], 'framework.py'), "rb").read(), os.path.join(sys.path[0], 'framework.py'), 'exec'))

from Testing import ZopeTestCase
from Products.CMFPlone.tests import PloneTestCase
from Products.CMFBibliographyAT.tests import setup
from ZODB.PersistentList import PersistentList
from Persistence import PersistentMapping

#ZopeTestCase.installProduct('DeadlockDebugger')
#import Products.DeadlockDebugger

class TestBibtexParsing(PloneTestCase.PloneTestCase):
    """
    """

    def afterSetUp(self):
        self._refreshSkinData()

    def testBibtexAuthorParsing(self):
        bibtool = self.portal.portal_bibliography
        p = bibtool.getParser('bibtex')
        source = open(setup.BIBTEX_TEST_MULTI_AUTHORS, 'r').read()
        source = p.preprocess(source)
        result = p.parseEntry(source)
        heckman =  {'middlename': 'J.',
                    'firstname' : 'James',
                    'lastname'  : 'Heckman'}
        carneiro = {'middlename': '',
                    'firstname' : 'Pedro',
                    'lastname'  : 'Carneiro'}
        self.assertTrue( len( result['authors'] ) == 2 )
        author1 = result['authors'][0]
        self.assertTrue(author1['middlename'] == carneiro['middlename'])
        self.assertTrue(author1['firstname'] == carneiro['firstname'])
        self.assertTrue(author1['lastname'] == carneiro['lastname'])
        author2 = result['authors'][1]
        self.assertTrue(author2['middlename'] == heckman['middlename'])
        self.assertTrue(author2['firstname'] == heckman['firstname'])
        self.assertTrue(author2['lastname'] == heckman['lastname'])

    def testBibtexInbookReferenceParsing(self):
        bibtool = self.portal.portal_bibliography
        p = bibtool.getParser('bibtex')
        source = open(setup.BIBTEX_TEST_INBOOKREFERENCES, 'r').read()
        ref = {
            'booktitle': 'In einem fiktiven Buch vor unserer Zeit',
            'title': 'Die Tage der Ankunft',
            'chapter': 'Die Tage der Ankunft',
            'publication_url': 'http://www.sunweavers.net/',
        }

        source = p.preprocess(source)
        result = p.parseEntry(source)

        for key in list(ref.keys()):
            self.assertTrue( key in result and (ref[key] == result[key]) )

    def testAnnoteParsing(self):
        bibtool = self.portal.portal_bibliography
        p = bibtool.getParser('bibtex')
        source = open(setup.BIBTEX_TEST_BIB, 'r').read()
        results = p.getEntries(source)
        self.assertTrue(results[-1]['annote'] == 'I really like it.')

    def testBibtexTypeFieldParsing(self):
        bibtool = self.portal.portal_bibliography
        p = bibtool.getParser('bibtex')
        source = open(setup.BIBTEX_TEST_TYPEFIELD, 'r').read()
        ref = {
            'publication_type': 'Doktorarbeit',
            'title': 'Mein Herr Doktor',
            'school': 'CAU Kiel',
            'institution': 'Ökologie-Zentrum',
        }

        source = bibtool.checkEncoding(source, 'iso-8859-15')
        source = p.preprocess(source)
        result = p.parseEntry(source)

        for key in list(ref.keys()):
            self.assertTrue( key in result and (ref[key] == result[key]) )

    def testBibtexTypeLastFieldTrailingKomma(self):
        bibtool = self.portal.portal_bibliography
        p = bibtool.getParser('bibtex')
        source = open(setup.BIBTEX_TEST_LASTFIELDKOMMA, 'r').read()

        source = bibtool.checkEncoding(source, 'iso-8859-15')
        source = p.preprocess(source)
        results = p.getEntries(source)

        # the last field in a bibtex entry always had a trailing ","
        self.assertTrue( len(results) == 2  )
        self.assertTrue( results[0]['institution'] == results[1]['institution']  )
        self.assertTrue( results[0]['publication_type'] == results[1]['publication_type']  )
        self.assertTrue( results[0]['publication_type'] == 'Doktorarbeit,,,' )

def test_suite():
    from unittest import TestSuite, makeSuite
    suite = TestSuite()
    suite.addTest(makeSuite(TestBibtexParsing))
    return suite

if __name__ == '__main__':
    framework()
